Pod::Spec.new do |spec|

  spec.name          = "CordialAppExtensions"
  spec.version       = "0.0.1"
  spec.summary       = "CordialAppExtensions summary."

  spec.description   = <<-DESC
	CordialAppExtensions description
  DESC

  spec.homepage      = "https://ymalinovsky@bitbucket.org/ymalinovsky/cordial-ios-appextensions.git"

  spec.license       = "MIT"

  spec.author        = "Cordial Experience, Inc."

  spec.platform      = :ios, "11.0"

  spec.swift_version = "4.2"

  spec.source        = { :git => "https://ymalinovsky@bitbucket.org/ymalinovsky/cordial-ios-appextensions.git", :tag => "#{spec.version}" }

  spec.source_files  = "CordialAppExtensions", "CordialAppExtensions/**/*.{swift}"

  spec.requires_arc  = true

end
